<?php

$dossier = opendir('../json_data/messages/');

$liste_elements = array();
$liste_reactions = array();

while($nom_fichier = readdir($dossier))
{
    if(in_array($nom_fichier,['.', '..', 'stickers_used']))
        continue;

    $messages_path = '../json_data/messages/' . 'L3_7f0dfaeeb7' . '/' . 'message_1.json';
    $fichier_json = json_decode(file_get_contents($messages_path));

    if(!property_exists($fichier_json, 'title'))
        continue;

    foreach($fichier_json->messages as $message)
    {
        if(!in_array($message->type, $liste_elements)){
            // var_dump($message);
            array_push($liste_elements, $message->type);
        }
        if(property_exists($message, 'reactions')) {
            foreach($message->reactions as $reaction) {
                if(!in_array($reaction->reaction, $liste_reactions)) {
                    var_dump($message);
                    var_dump($messages_path);
                    array_push($liste_reactions, $reaction->reaction);
                }
            }
        }
    }
}

var_dump($liste_elements);
var_dump($liste_reactions);