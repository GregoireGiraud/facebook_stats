<?php


?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <title><?=$thread->title?></title>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </head>
    <body>
        <?php
        // goto bug;
        ?>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <?= Functions::display_table_head("Catégories"); ?>
                <tbody>
                    <tr>
                        <th scope="row">Still participant</th>
                        <?php foreach($persons_stats as $person_stats) {
                            echo '<td class="' . Functions::display_proper_style($person_stats['still_member']) . '">' . ($person_stats['still_member'] ? 'Yes' : 'Nope') . '</td>';
                        }
                        echo '<td>X</td>';
                         ?>
                    </tr>
                    <?= Functions::display_classic_row('first_day_posted_on', 'times') ?>
                    <?= Functions::display_classic_row('last_day_posted_on', 'times') ?>
                    <?= Functions::display_stats_percentage_row('total_msg_nb') ?>
                    <?= Functions::display_stats_percentage_row('reg_msg', 'message_types_number') ?>
                    <?= Functions::display_stats_percentage_row('messages_reacted_to', 'reactions') ?>
                    <?= Functions::display_stats_percentage_row('reactions_done', 'reactions') ?>
                    <?= Functions::display_stats_percentage_row('reactions_received', 'reactions') ?>
                    <?= Functions::display_stats_percentage_row('most_reacted_to', 'reactions') ?>
                    <?= Functions::display_percentage_row(20, 'reactions_per_message', 'reactions') ?>
                    <?= Functions::display_percentage_row(0.8, 'reactions_per_messages_reacted_to', 'reactions') ?>
                    <?= Functions::display_stats_percentage_row('nb_days_posted_on', 'times') ?>
                    <?= Functions::display_stats_percentage_row('photos', 'message_types_number') ?>
                    <?= Functions::display_stats_percentage_row('videos', 'message_types_number') ?>
                    <?= Functions::display_stats_percentage_row('gifs', 'message_types_number') ?>
                    <?= Functions::display_stats_percentage_row('Share', 'message_types_number') ?>
                    <?= Functions::display_stats_percentage_row('sticker', 'message_types_number') ?>
                    <?= Functions::display_stats_percentage_row('Subscribe', 'message_types_number') ?>
                    <?= Functions::display_stats_percentage_row('Unsubscribe', 'message_types_number') ?>
                    <?= Functions::display_stats_percentage_row('Call', 'message_types_number') ?>
                    <?= Functions::display_stats_percentage_row('Plan', 'message_types_number') ?>
                </tbody>
            </table>
        </div>
            <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <?= Functions::display_table_head("Jours"); ?>
                <tbody>
                    <?php Functions::display_stats_percentage_row_array('weekdays'); ?>
                </tbody>
            </table>
        </div>
        <?php end: ?>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <?= Functions::display_table_head("Heures"); ?>
                <tbody>
                    <?php Functions::display_stats_percentage_row_array('hours'); ?>
                </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <?= Functions::display_table_head("Participants"); ?>
                <tbody>
                    <?php Functions::display_reactions_senders('reaction_senders', 'reactions'); ?>
                </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <?= Functions::display_table_head("Participants"); ?>
                <tbody>
                    <?php Functions::display_reaction_types('reactions_done_type'); ?>
                </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <?= Functions::display_table_head("Participants"); ?>
                <tbody>
                    <?php Functions::display_reaction_types('reactions_received_type'); ?>
                </tbody>
            </table>
        </div>
        <?php Functions::display_reactions_senders_types() ?>
    </body>
</html>