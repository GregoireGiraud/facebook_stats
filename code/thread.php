<?php

require 'class/Functions.class.php';
require 'class/Thread.class.php';
require 'class/Thread_stats.class.php';
require 'config.php';

if(!isset($_GET['group_name']))
    header('Location: index.php');

$thread = new Thread($_CONFIG['paths']['messages'] . $_GET['group_name'] . '/' . 'message_1.json');
$stats = $thread->get_stats();
$persons_stats = $stats->persons_stats;
$thread_stats = $stats->thread_stats;
$persons = count($persons_stats);

require 'templates/thread.php';

?>