<?php

Class Thread {

    public $participants;
    public $messages;
    public $title;
    public $still_participant;
    public $thread_type;
    public $thread_path;

    public function __construct($thread_path) {
        $json_file = json_decode(file_get_contents($thread_path));

        $this->participants = $json_file->participants;
        $this->messages = $json_file->messages;
        $this->title = $json_file->title;
        $this->still_participant = $json_file->is_still_participant;
        $this->thread_type = $json_file->thread_type;
        $this->thread_path = $json_file->thread_path;
    }

    public function first_message() {
        return end($this->messages);
    }
    public function first_message_date($format = 'd/m/Y H:i:s') {
        return Functions::ms_timestamp_to_date_with_format($this->first_message()->timestamp_ms);
    }

    public function get_stats($stats_wanted = Thread_stats::FIELD_GROUPS) {
        return new Thread_stats($this->messages, $this->participants, $stats_wanted);
    }

}

?>