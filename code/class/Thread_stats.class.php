<?php

Class Thread_stats {

    public $thread_stats = self::EMPTY_PERSON;
    public $persons_stats = [];

    const FIELD_GROUPS = ['message_types_number', 'reactions', 'weekdays', 'hours', 'months', 'years', 'years_months', 'days', 'times'];
    const INCREMENT_GROUPS = ['message_types_number', 'reactions', 'weekdays', 'hours', 'months', 'years', 'years_months', 'days'];
    const TIME_GROUPS = ['times', 'weekdays', 'hours', 'months', 'years', 'years_months', 'days'];

    const POURCENTAGE_FIELDS = ['total_msg_nb'];
    const POURCENTAGE_GROUP_FIELDS =  ['message_types_number', 'reactions'];
    const POURCENTAGE_GROUP_FIELDS_ARRAYS =  ['weekdays', 'hours', 'months', 'years', 'years_months', 'days'];
    const POURCENTAGE_FIELDS_IN_GROUPS =  ['times' => ['biggest_wait', 'days_since_creation', 'days_since_last_message', 'nb_days_posted_on']];

    const REACTIONS_ARRAYS = ['reaction_senders', 'reactions_done_type', 'reactions_received_type'];
    const REACTIONS_DOUBLE_ARRAYS = ['reaction_senders_type'];

    const VALID_CODES = ['ð', 'ð', 'ð®', 'ð', 'ð¢', 'ð'];

    const ODD_MESSAGE_TYPES = ['Share', 'Call', 'Plan', 'Subscribe', 'Unsubscribe'];

    const EMPTY_PERSON = [

        'name' => "",
        'still_member' => false,
        'array_index' => 0,
        'total_msg_nb' => 0,
        'started_conversation' => false,

        'message_types_number' => [
            'reg_msg' => 0,
            'photos' => 0,
            'videos' => 0,
            'gifs' => 0,
            'sticker' => 0,
            'Share' => 0,
            'Subscribe' => 0,
            'Unsubscribe' => 0,
            'Call' => 0,
            'Plan'=> 0
        ],

        'reactions' => [
            'messages_reacted_to' => 0,
            'reactions_received' => 0,
            'reactions_done' => 0,
            'most_reacted_to' => 0,
            "reactions_per_message" => 0,
            "reactions_per_messages_reacted_to" => 0,
            "reaction_senders" => [],
            "reaction_senders_type" => [
                'ð' => [],
                'ð' => [],
                'ð®' => [],
                'ð' => [],
                'ð¢' => [],
                'ð' => [],
                "angry" => []
            ],
            "reactions_received_type" => [
                'ð' => 0,
                'ð' => 0,
                'ð®' => 0,
                'ð' => 0,
                'ð¢' => 0,
                'ð' => 0,
                "angry" => 0
            ],
            "reactions_done_type" => [
                'ð' => 0,
                'ð' => 0,
                'ð®' => 0,
                'ð' => 0,
                'ð¢' => 0,
                'ð' => 0,
                "angry" => 0
            ]
        ],
        'weekdays' => [
            'weekdays_posted_on' => [0,0,0,0,0,0,0],
            'messages_on_weekdays' => [0,0,0,0,0,0,0]
        ],
        'hours' => [
            'hours_posted_on' => [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            'messages_on_hours' => [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        ],
        'months' => [
            'months_posted_on' => [0,0,0,0,0,0,0,0,0,0,0,0],
            'messages_on_months' => [0,0,0,0,0,0,0,0,0,0,0,0]
        ],
        'years' => [
            'years_posted_on' => [],
            'messages_on_years' => []
        ],
        'years_months' => [
            'years_months_posted_on' => [],
            'messages_on_years_months' => []
        ],
        'days' => [
            'days_posted_on' => [],
            'messages_on_days' => []
        ],

        'times' => [
            'biggest_wait' => 0,
            'days_since_creation' => 0,
            'days_since_last_message' => 0,
            'last_timestamp' => 0,
            'last_day_posted_on' => '',
            'first_day_posted_on' => '',
            'nb_days_posted_on' => 0
        ],
    ];

    private function empty_reaction_senders($count) {
        $reaction_senders = [];
        for($i=0; $i<$count; $i++) {
            array_push($reaction_senders, 0);
        }
        return $reaction_senders;
    }

    private function get_participant_index($name) {
        foreach($this->persons_stats as $person) {
            if($person['name'] === $name){
                return $person['array_index'];
            }
        }
        return false;
    }

    private function increment_stats($participant_index, $field, $field_group=false, $other_index=false, $last_index=false) {
        if($field_group === false) {
            $this->thread_stats[$field] += 1;
            $this->persons_stats[$participant_index][$field] += 1;
        }
        elseif($other_index === false) {
            $this->thread_stats[$field_group][$field] += 1;
            $this->persons_stats[$participant_index][$field_group][$field] += 1;
        }
        elseif($last_index === false) {
            $this->thread_stats[$field_group][$field][$other_index] += 1;
            $this->persons_stats[$participant_index][$field_group][$field][$other_index] += 1;
        }
        else {
            $this->thread_stats[$field_group][$field][$other_index][$last_index] += 1;
            $this->persons_stats[$participant_index][$field_group][$field][$other_index][$last_index] += 1;
        }
    }

    private function update_stats($value, $participant_index, $field, $field_group=false) {
        if($field_group === false) {
            $this->thread_stats[$field] = $value;
            $this->persons_stats[$participant_index][$field] = $value;
        }
        else {
            $this->thread_stats[$field_group][$field] = $value;
            $this->persons_stats[$participant_index][$field_group][$field] = $value;
        }
    }

    private function are_time_groups_wanted($groups_wanted) {
        foreach($groups_wanted as $group_wanted) {
            if(in_array($group_wanted,  self::TIME_GROUPS))
                return true;
        }
        return false;
    }

    private function set_percentage($participant_index, $field, $field_group = false, $other_index = false, $last_index = false, $precision=2) {
        $pourcentage_field = $field . '_pourcentage';
        if($field_group === false) {
            $this->thread_stats[$pourcentage_field] = Functions::set_percentage($this->thread_stats[$field], $this->thread_stats['total_msg_nb'], $precision);
            $this->persons_stats[$participant_index][$pourcentage_field] = Functions::set_percentage($this->persons_stats[$participant_index][$field], $this->thread_stats[$field], $precision);
        }
        elseif($other_index === false) {
            $this->thread_stats[$field_group][$pourcentage_field] = Functions::set_percentage($this->thread_stats[$field_group][$field], $this->thread_stats['total_msg_nb'], $precision);
            $this->persons_stats[$participant_index][$field_group][$pourcentage_field] = Functions::set_percentage($this->persons_stats[$participant_index][$field_group][$field], $this->thread_stats[$field_group][$field], $precision);
        }
        elseif($last_index === false) {
            $this->thread_stats[$field_group][$pourcentage_field][$other_index] = Functions::set_percentage($this->thread_stats[$field_group][$field][$other_index], $this->thread_stats['total_msg_nb'], $precision);
            $this->persons_stats[$participant_index][$field_group][$pourcentage_field][$other_index] = Functions::set_percentage($this->persons_stats[$participant_index][$field_group][$field][$other_index], $this->thread_stats[$field_group][$field][$other_index], $precision);
        }
        else {
            $this->thread_stats[$field_group][$pourcentage_field][$other_index][$last_index] = Functions::set_percentage($this->thread_stats[$field_group][$field][$other_index][$last_index], $this->thread_stats['total_msg_nb'], $precision);
            $this->persons_stats[$participant_index][$field_group][$pourcentage_field][$other_index][$last_index] = Functions::set_percentage($this->persons_stats[$participant_index][$field_group][$field][$other_index][$last_index], $this->thread_stats[$field_group][$field][$other_index][$last_index], $precision);
        }
    }

    private function prepare_pourcentage_set_for_arrays($index, $field, $field_group) {
        for($i=0; $i<count($this->persons_stats[$index][$field_group][$field]); $i++) {
            $this->set_percentage($index, $field, $field_group, $i);
        }
    }
    private function prepare_pourcentage_set_for_arrays_associative($index, $field, $field_group) {
        foreach($this->persons_stats[$index][$field_group][$field] as $key=>$value) {
            $this->set_percentage($index, $field, $field_group, $key);
        }
    }
    private function prepare_pourcentage_set_for_double_arrays($index, $field, $field_group) {
        foreach($this->persons_stats[$index][$field_group][$field] as $key=>$value) {
            for($j=0; $j<count($this->persons_stats[$index][$field_group][$field][$key]); $j++) {
                $this->set_percentage($index, $field, $field_group, $key, $j);
            }
        }
    }

    private function add_participant($name, $time, $empty_reaction_senders) {
        $participant_index = count($this->persons_stats);

        $person_stats = self::EMPTY_PERSON;
        $person_stats['reactions']['reaction_senders'] = $empty_reaction_senders;
        foreach($person_stats['reactions']['reaction_senders_type'] as &$type) {
            $type = $empty_reaction_senders;
        }

        $person_stats['name'] = $name;
        $person_stats['array_index'] = count($this->persons_stats);
        $person_stats['times']['last_day_posted_on'] = Functions::ms_timestamp_to_date_with_format($time);
        array_push($this->persons_stats, $person_stats);
    }

    public function __construct($messages, $thread_participants, $groups_wanted = self::FIELD_GROUPS) {

        $participants_number = count($thread_participants);
        $empty_reaction_senders = $this->empty_reaction_senders($participants_number);
        $this->thread_stats['reactions']['reaction_senders'] = $empty_reaction_senders;
        foreach($this->thread_stats['reactions']['reaction_senders_type'] as &$type) {
            $type = $empty_reaction_senders;
        }

        $time_groups_wanted = $this->are_time_groups_wanted($groups_wanted);
        if($time_groups_wanted) {
            $time_since_last_msg = time() * 1000 - $messages[0]->timestamp_ms;
        }

        $this->thread_stats['name'] = 'All';
        $this->thread_stats['still_member'] = true;
        $this->thread_stats['array_index'] = INF;
        $this->thread_stats['total_msg_nb'] = count($messages);
        $this->thread_stats['times']['last_day_posted_on'] = Functions::ms_timestamp_to_date_with_format($messages[0]->timestamp_ms);
        $this->thread_stats['times']['first_day_posted_on'] = Functions::ms_timestamp_to_date_with_format(end($messages)->timestamp_ms);

        foreach($messages as $message) {
            $participant_index = $this->get_participant_index($message->sender_name);

            if($participant_index === false) {
                $this->add_participant($message->sender_name, $message->timestamp_ms, $empty_reaction_senders);
            }

            $this->persons_stats[$participant_index]['total_msg_nb'] += 1;

            if($time_groups_wanted) {
                $current_datetime = Functions::datetime_from_ms_timestamp($message->timestamp_ms);
                $this->persons_stats[$participant_index]['times']['first_day_posted_on'] = Functions::ms_timestamp_to_date_with_format($message->timestamp_ms);
                if(!isset($prev_timestamp)) {
                    if(in_array('times', $groups_wanted)) {
                        $biggest_wait = $time_since_last_msg;
                        $this->increment_stats($participant_index, 'nb_days_posted_on', 'times');
                        $this->increment_stats($participant_index, 'weekdays_posted_on', 'weekdays', $current_datetime->format('N')-1);
                    }
                } else {
                    if(in_array('times', $groups_wanted)) {
                        $biggest_wait = max($biggest_wait, abs($prev_timestamp-$message->timestamp_ms));
                    }
                    if($current_datetime->format('Y-m-d') !== Functions::ms_timestamp_to_date_with_format($prev_timestamp, 'Y-m-d')) {
                        if(in_array('days', $groups_wanted) || in_array('weekdays', $groups_wanted)) {
                            $this->increment_stats($participant_index, 'weekdays_posted_on', 'weekdays', $current_datetime->format('N')-1);
                        }
                        if(in_array('times', $groups_wanted)) {
                            $this->increment_stats($participant_index, 'nb_days_posted_on', 'times');
                        }
                    }
                    elseif($current_datetime->format('Y-m-d') !== Functions::ms_timestamp_to_date_with_format($this->persons_stats[$participant_index]['times']['last_timestamp'], 'Y-m-d')) {
                        if(in_array('days', $groups_wanted) || in_array('weekdays', $groups_wanted)) {
                            $this->persons_stats[$participant_index]['weekdays']['weekdays_posted_on'][$current_datetime->format('N')-1] +=1 ;
                        }
                        if(in_array('times', $groups_wanted)) {
                            $this->persons_stats[$participant_index]['times']['nb_days_posted_on'] += 1;
                        }
                    }
                    if($current_datetime->format('Y-m-d G') !== Functions::ms_timestamp_to_date_with_format($prev_timestamp, 'Y-m-d G')) {
                        if(in_array('hours', $groups_wanted) || in_array('hours', $groups_wanted)) {
                            $this->increment_stats($participant_index, 'hours_posted_on', 'hours', $current_datetime->format('G'));
                        }
                    }
                    elseif($current_datetime->format('Y-m-d G') !== Functions::ms_timestamp_to_date_with_format($this->persons_stats[$participant_index]['times']['last_timestamp'], 'Y-m-d G')) {
                        if(in_array('days', $groups_wanted) || in_array('hours', $groups_wanted)) {
                            $this->persons_stats[$participant_index]['hours']['hours_posted_on'][$current_datetime->format('G')] +=1 ;
                        }
                    }
                }

                $prev_timestamp = $message->timestamp_ms;
                $this->persons_stats[$participant_index]['times']['last_timestamp'] = $message->timestamp_ms;
            }

            foreach($groups_wanted as $group_wanted) {
                if(in_array($group_wanted, self::INCREMENT_GROUPS)) {
                    switch($group_wanted) {

                        case 'weekdays':
                            $this->increment_stats($participant_index, 'messages_on_weekdays', $group_wanted, $current_datetime->format('N')-1);
                            break;

                        case 'hours':
                            $this->increment_stats($participant_index, 'messages_on_hours', $group_wanted, $current_datetime->format('G'));
                            break;

                        // case 'months':
                        //     $this->increment_stats($participant_index, 'messages_on_months', $group_wanted, $current_datetime->format('N')-1);
                        //     break;

                        // case 'years':
                        //     $this->increment_stats($participant_index, 'messages_on_years', $group_wanted, $current_datetime->format('N')-1);
                        //     break;

                        // case 'years_months':
                        //     $this->increment_stats($participant_index, 'messages_on_years_months', $group_wanted, $current_datetime->format('N')-1);
                        //     break;

                        // case 'days':
                        //     $this->increment_stats($participant_index, 'messages_on_days', $group_wanted, $current_datetime->format('N')-1);
                        //     break;

                        case 'message_types_number':
                            if(in_array($message->type, self::ODD_MESSAGE_TYPES)) {
                                $this->increment_stats($participant_index, $message->type, $group_wanted);
                            } elseif(property_exists($message, 'photos')) {
                                $this->increment_stats($participant_index, 'photos', $group_wanted);
                            } elseif(property_exists($message, 'videos')) {
                                $this->increment_stats($participant_index, 'videos', $group_wanted);
                            } elseif(property_exists($message, 'gifs')) {
                                $this->increment_stats($participant_index, 'gifs', $group_wanted);
                            } elseif(property_exists($message, 'sticker')) {
                                $this->increment_stats($participant_index, 'sticker', $group_wanted);
                            } else {
                                $this->increment_stats($participant_index, 'reg_msg', $group_wanted);
                            }
                            break;

                        case 'reactions':
                            if(property_exists($message, 'reactions')) {
                                $this->increment_stats($participant_index, 'messages_reacted_to', $group_wanted);

                                $this->persons_stats[$participant_index][$group_wanted]['most_reacted_to'] = max($this->persons_stats[$participant_index][$group_wanted]['most_reacted_to'], count($message->reactions));
                                $this->thread_stats[$group_wanted]['most_reacted_to'] = max($this->thread_stats[$group_wanted]['most_reacted_to'], count($message->reactions));

                                foreach($message->reactions as $reaction) {
                                    $reaction_sender = $this->get_participant_index($reaction->actor);
                                    if($reaction_sender === false) {
                                        foreach($this->persons_stats as $person_stats) {
                                        }
                                        $this->add_participant($reaction->actor, time()*1000, $empty_reaction_senders);
                                        $reaction_sender = $this->get_participant_index($reaction->actor);
                                    }

                                    $this->increment_stats($participant_index, 'reactions_received', $group_wanted);
                                    $this->increment_stats($reaction_sender, 'reactions_done', $group_wanted);
                                    $this->increment_stats($participant_index, 'reaction_senders', $group_wanted, $reaction_sender);

                                    if(in_array($message->reactions[0]->reaction, self::VALID_CODES)) {
                                        $this->increment_stats($participant_index, 'reactions_received_type', $group_wanted, $message->reactions[0]->reaction);
                                        $this->increment_stats($reaction_sender, 'reactions_done_type', $group_wanted, $message->reactions[0]->reaction);
                                        $this->increment_stats($participant_index, 'reaction_senders_type', $group_wanted, $message->reactions[0]->reaction, $reaction_sender);
                                    }
                                }
                            }
                    }
                }
            }
        }

        foreach($this->persons_stats as $person) {
            foreach($thread_participants as $participant) {
                if($person['name'] === $participant->name){
                    $this->persons_stats[$this->get_participant_index($person['name'])]['still_member'] = true;
                    break;
                }
            }
        }
        foreach($thread_participants as $participant) {
            if($this->get_participant_index($participant->name) === false) {
                $muted_participant = self::EMPTY_PERSON;
                $muted_participant['name'] = $participant->name;
                $muted_participant['still_member'] = $participant->name;
                $muted_participant['array_index'] = end($this->persons_stats)['array_index'] + 1;
                $muted_participant['reactions']['reaction_senders'] = $empty_reaction_senders;
                foreach($muted_participant['reactions']['reaction_senders_type'] as &$type) {
                    $type = $empty_reaction_senders;
                }
                array_push($this->persons_stats, $muted_participant);
            }
        }



        if(!$messages[0]->content == 'Bienvenue \u00c3\u00a0 vous deux sur Messenger.') {
            $this->persons_stats[$this->get_participant_index($messages[0]->sender_name)]['started_conversation'] = true;
        } else {
            $this->persons_stats[$this->get_participant_index($messages[1]->sender_name)]['started_conversation'] = true;
        }

        $this->thread_stats['reactions']['reactions_per_message'] = Functions::set_percentage($this->thread_stats['reactions']['messages_reacted_to'], $this->thread_stats['total_msg_nb']);
        $this->thread_stats['reactions']['reactions_per_messages_reacted_to'] = Functions::set_percentage($this->thread_stats['reactions']['reactions_received'], $this->thread_stats['reactions']['messages_reacted_to']);

        $others_received = 0;
        $others_done = 0;
        $angry_senders = [];
        foreach($this->thread_stats['reactions']['reactions_received_type'] as $emoji_code => $value) {
            $others_received += $value;
            $others_done += $this->thread_stats['reactions']['reactions_done_type'][$emoji_code];
        }
        $this->thread_stats['reactions']['reactions_received_type']['angry'] = $this->thread_stats['reactions']['reactions_received'] - $others_received;
        $this->thread_stats['reactions']['reactions_done_type']['angry'] = $this->thread_stats['reactions']['reactions_done'] - $others_done;

        foreach($this->persons_stats as &$person_stats) {
            $index = $person_stats['array_index'];

            $person_stats['reactions']['reactions_per_message'] = Functions::set_percentage($person_stats['reactions']['messages_reacted_to'], $person_stats['total_msg_nb']);
            $person_stats['reactions']['reactions_per_messages_reacted_to'] = Functions::set_percentage($person_stats['reactions']['reactions_received'], $person_stats['reactions']['messages_reacted_to']);

            $others_received = 0;
            $others_done = 0;
            $angry_senders = [];
            foreach($person_stats['reactions']['reactions_received_type'] as $emoji_code => $value) {
                $others_received += $value;
                $others_done += $person_stats['reactions']['reactions_done_type'][$emoji_code];
            }
            $person_stats['reactions']['reactions_received_type']['angry'] = $person_stats['reactions']['reactions_received'] - $others_received;
            $person_stats['reactions']['reactions_done_type']['angry'] = $person_stats['reactions']['reactions_done'] - $others_done;

            foreach(self::POURCENTAGE_FIELDS as $field) {
                $this->set_percentage($index, $field);
            }
            foreach(self::POURCENTAGE_GROUP_FIELDS as $group_field) {
                foreach($person_stats[$group_field] as $field => $value) {
                    if(in_array($field, self::REACTIONS_ARRAYS) || in_array($field, self::REACTIONS_DOUBLE_ARRAYS))
                        continue;
                    $this->set_percentage($index, $field, $group_field);
                }
            }
            foreach(self::POURCENTAGE_GROUP_FIELDS_ARRAYS as $group_field) {
                foreach($person_stats[$group_field] as $field => $value) {
                    $this->prepare_pourcentage_set_for_arrays($index, $field, $group_field);
                }
            }
            $this->prepare_pourcentage_set_for_arrays($index, 'reaction_senders', 'reactions');
            $this->prepare_pourcentage_set_for_arrays_associative($index, 'reactions_done_type', 'reactions');
            $this->prepare_pourcentage_set_for_arrays_associative($index, 'reactions_received_type', 'reactions');
            $this->prepare_pourcentage_set_for_double_arrays($index, 'reaction_senders_type', 'reactions');
            foreach(self::POURCENTAGE_FIELDS_IN_GROUPS as $group_field => $fields) {
                foreach($person_stats[$group_field] as $field => $value) {
                    if(in_array($field, $fields)) {
                        $this->set_percentage($index, $field, $group_field);
                    }
                }
            }
        }
    }
}