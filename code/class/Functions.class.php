<?php

Class Functions {
    const DAYS = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
    const ASSOCIATION_DATA_TITLES = [
        "still_member" => "Toujours dans la conversation",
        "total_msg_nb" => "Nombre total de messages",
        "first_day_posted_on" => "Premier message le",
        "last_day_posted_on" => "Dernier message le",
        "reg_msg" => "Messages classiques",
        "messages_reacted_to" => "Messages avec réactions",
        "reactions_received" => "Réactions reçues",
        "reactions_done" => "Réactions faites",
        "most_reacted_to" => "Plus grand nombre de réactions",
        "reactions_per_message" => "Réactions par message",
        "reactions_per_messages_reacted_to" => "Nombre de réactions par messages réagis",
        "nb_days_posted_on" => "Nombre de jours postés",
        "photos" => "Photos",
        "videos" => "Vidéos",
        "gifs" => "Gifs",
        "Share" => "Liens",
        "sticker" => "Stickers",
        "Subscribe" => "Ajouts",
        "Unsubscribe" => "Enlèvements",
        "Call" => "Appels",
        "Plan" => "Plans"
    ];
    const ASSOCIATION_EMOJIS_CODE = [
    'ð' => '<img alt="😆" src="https://static.xx.fbcdn.net/images/emoji.php/v9/z6e/1/128/1f606.png">',
    'ð' => '<img alt="👍"  src="https://static.xx.fbcdn.net/images/emoji.php/v9/z96/1/128/1f44d.png">',
    'ð®' => '<img alt="😮" src="https://static.xx.fbcdn.net/images/emoji.php/v9/z5b/1/128/1f62e.png">',
    'ð' => '<img alt="😍" src="https://static.xx.fbcdn.net/images/emoji.php/v9/z9c/1/128/1f60d.png">',
    'ð¢' => '<img alt="😢" src="https://static.xx.fbcdn.net/images/emoji.php/v9/za8/1/128/1f622.png">',
    'ð' => '<img alt="👎"  src="https://static.xx.fbcdn.net/images/emoji.php/v9/z17/1/128/1f44e.png">',
    "angry" => '<img style="width:128px;" alt="😠" class="_1ift _5m3a img" src="https://static.xx.fbcdn.net/images/emoji.php/v9/z28/3/128/1f620.png">'
    ];


    public static function ms_timestamp_to_date_with_format($ms_ts, $format='d/m/Y H:i:s') {
        return self::datetime_from_ms_timestamp($ms_ts)->format($format);
    }

    public static function datetime_from_ms_timestamp($ms_ts) {
        return new DateTime('@' . round($ms_ts/1000));
    }

    public static function division_extended_for_zero($a, $b) {
        // var_dump($a);
        // var_dump($b);
        return $b !==0 ? $a/$b : 0;
    }

    public static function set_percentage($a, $b, $precision=2) {
        return round(100*self::division_extended_for_zero($a, $b), $precision);
    }

    public static function get_days_from_seconds($seconds) {
        return ceil($seconds/86400);
    }

    public static function weekday_number_to_day($number) {
        return self::DAYS[$number];
    }

    public static function number_to_lign_title($group_field, $number) {
        switch($group_field) {
            case 'weekdays':
                return self::weekday_number_to_day($number);
            case 'hours':
                return $number . 'h';
        }
    }

    /**
     * Fonction permettant de mettre une couleur particulière sur un pourcentage
     * Il est appelée juste au moement d'écrire la classe de la cellule
     * Dans l'ordre croissant : rouge / jaune / gris / bleu / vert
     * @param  [int] $pourcentage
     * @param  [int] $number_rows [en théorie le nombre de lignes, afin de pondérer le pourcentage par raport à la moyenne. En soit, il est possible de mettre n'importe quoi afin d'ajuster certaines couleurs]
     */
    public static function display_percentage_style($pourcentage, $number_rows) {
        switch ($pourcentage)
        {
            case ('undefined'):
                break;
            case ($pourcentage<20/$number_rows):
                echo 'table-danger';
                break;
            case ($pourcentage<60/$number_rows):
                echo 'table-active';
                break;
            case ($pourcentage<100/$number_rows):
                echo 'table-warning';
                break;
            case ($pourcentage<200/$number_rows):
                echo 'table-info';
                break;
            case ($pourcentage>=200/$number_rows):
                echo 'table-success';
                break;
        }
    }

    public static function display_proper_style($bool) {
        return $bool ? "table-success" : "table-danger";
    }

    public static function display_stats_percentage_row($field, $group_field = false) {
        global $persons_stats, $thread_stats, $persons;
        $pourcentage_field = $field. '_pourcentage';

        ?>
        <tr>
            <th><?=self::ASSOCIATION_DATA_TITLES[$field]?></th>
            <?php foreach($persons_stats as $person_stats) {
                if($group_field === false) { ?>
                    <td class="<?=self::display_percentage_style($person_stats[$pourcentage_field], $persons)?>">
                            <?= $person_stats[$field] . '(' . $person_stats[$pourcentage_field] . '%)'; ?>
                    </td> <?php
                } else { ?>
                    <td class="<?=self::display_percentage_style($person_stats[$group_field][$pourcentage_field], $persons)?>">
                        <?= $person_stats[$group_field][$field] . '(' . $person_stats[$group_field][$pourcentage_field] . '%)' ?>
                    </td> <?php
                }
            }
            if($group_field === false) { ?>
                <td class="<?=self::display_percentage_style($thread_stats[$pourcentage_field], 2)?>">
                    <?= $thread_stats[$field] . '(' . $thread_stats[$pourcentage_field] . '%)' ?>
                </td> <?php
            } else { ?>
                <td class="<?=self::display_percentage_style($thread_stats[$group_field][$pourcentage_field], 2)?>">
                    <?= $thread_stats[$group_field][$field] . '(' . $thread_stats[$group_field][$pourcentage_field] . '%)' ?>
                </td> <?php
            } ?>
        </tr>
        <?php
    }

    public static function display_percentage_row($divider, $field, $group_field = false) {
        global $persons_stats, $thread_stats;
        ?>
        <tr>
            <th><?=self::ASSOCIATION_DATA_TITLES[$field]?></th>
            <?php foreach($persons_stats as $person_stats) {
                if($group_field === false) { ?>
                    <td class="<?=self::display_percentage_style($person_stats[$field], $divider)?>">
                            <?= $person_stats[$field] . "%"; ?>
                    </td> <?php
                } else { ?>
                    <td class="<?=self::display_percentage_style($person_stats[$group_field][$field], $divider)?>">
                        <?= $person_stats[$group_field][$field] . "%"; ?>
                    </td> <?php
                }
            }

            if($group_field === false) { ?>
                <td class="<?=self::display_percentage_style($thread_stats[$field], $divider)?>">
                    <?= $thread_stats[$field] .'%'; ?>
                </td> <?php
            } else { ?>
                <td class="<?=self::display_percentage_style($thread_stats[$group_field][$field], $divider)?>">
                    <?= $thread_stats[$group_field][$field] .'%'; ?>
                </td> <?php
            }
            ?>
        </tr>
        <?php
    }

    public static function display_classic_row($field, $group_field = false) {
        global $persons_stats, $thread_stats;
        ?>
        <tr>
            <th><?=self::ASSOCIATION_DATA_TITLES[$field]?></th>
            <?php foreach($persons_stats as $person_stats) {
                if($group_field === false) { ?>
                    <td> <?= $person_stats[$field] ?> </td> <?php
                } else { ?>
                    <td> <?= $person_stats[$group_field][$field] ?> </td> <?php
                }
            } ?>
            <td>
                <?php
                if($group_field === false) {
                    echo $thread_stats[$field];
                } else {
                    echo $thread_stats[$group_field][$field];
                } ?>
            </td>
        </tr>
        <?php
    }

    public static function display_stats_percentage_row_array($group_field) {
        global $persons_stats, $thread_stats, $persons;
        $messages_on = 'messages_on_' . $group_field;
        $messages_on_percentage = $messages_on . '_pourcentage';
        $posted_on = $group_field . '_posted_on';
        $posted_on_percentage = $posted_on . '_pourcentage';
        $rows = count($persons_stats[0][$group_field][$messages_on]);

        for($i=0; $i<$rows; $i++) {
            echo '<tr><th>' . Functions::number_to_lign_title($group_field, $i) . '</th>';
            foreach($persons_stats as $person_stats) { ?>
                <td class="<?=self::display_percentage_style($person_stats[$group_field][$messages_on_percentage][$i], $persons)?>">
                    <?= $person_stats[$group_field][$messages_on][$i] . '(' . $person_stats[$group_field][$messages_on_percentage][$i] . '%) sur ' . $person_stats[$group_field][$posted_on][$i] . '(' . $person_stats[$group_field][$posted_on_percentage][$i] . '%)' ?>
                </td> <?php
            } ?>
            <td class="<?=self::display_percentage_style($thread_stats[$group_field][$messages_on_percentage][$i], $rows)?>">
                <?= $thread_stats[$group_field][$messages_on][$i] . '(' . $thread_stats[$group_field][$messages_on_percentage][$i] . '%) sur ' . $thread_stats[$group_field][$posted_on][$i] . '(' . $thread_stats[$group_field][$posted_on_percentage][$i] . '%)' ?>
            </td> <?php
            echo '<tr>';
        }
    }

    public static function display_table_head($label) {
        global $persons_stats;
        ?>
            <thead>
                <tr>
                    <th scope="col"><?=$label?></th>
                    <?php foreach($persons_stats as $person_stats) {
                        echo '<th scope="col">' . $person_stats['name'] . '</th>';
                    } ?>
                    <th scope="col">Totaux</th>
                </tr>
            </thead>
        <?php
    }

    public static function display_reactions_senders($field, $group_field) {
        global $persons_stats;
        $pourcentage_field = $field . '_pourcentage';
        $rows = count($persons_stats[0][$group_field][$field]);
        foreach($persons_stats as $person_stats) {
            ?>
            <tr>
                <td><?=$person_stats['name'] . ' (sur ' . $person_stats['reactions']['reactions_received'] . ' reçues)'?></td>
                <?php for($i=0; $i<$rows; $i++) {
                    ?>
                    <td class="<?=self::display_percentage_style($person_stats[$group_field][$pourcentage_field][$i], $rows)?>">
                        <?=$person_stats[$group_field][$field][$i] . '(' . $person_stats[$group_field][$pourcentage_field][$i] .'%)' ?>
                    </td>
                    <?php
                } ?>
            </tr>
            <?php
        }
    }

    public static function display_reaction_types($field) {
        global $persons_stats, $thread_stats;
        $association = self::ASSOCIATION_EMOJIS_CODE;
        $pourcentage_field = $field . '_pourcentage';
        $rows = count($association);
        foreach($association as $emoji_code => $emoji_image) {
            echo '<tr><th>' . $emoji_image . ' ' . $field . '</th>';
            foreach($persons_stats as $person_stats) { ?>
                <td class="<?=self::display_percentage_style($person_stats['reactions'][$pourcentage_field][$emoji_code], $rows)?>">
                    <?= $person_stats['reactions'][$field][$emoji_code] . '(' . $person_stats['reactions'][$pourcentage_field][$emoji_code] . '%)'?>
                </td> <?php
            } ?>
            <td class="<?=self::display_percentage_style($thread_stats['reactions'][$pourcentage_field][$emoji_code], $rows)?>">
                <?= $thread_stats['reactions'][$field][$emoji_code] . '(' . $thread_stats['reactions'][$pourcentage_field][$emoji_code] . '%)'?>
            </td> <?php
            echo '<tr>';
        }
    }

    public static function display_reactions_senders_types() {
        global $persons_stats;
        $group_field = 'reactions';
        $field = "reaction_senders_type";
        $pourcentage_field = 'reaction_senders_type_pourcentage';
        $rows = count($persons_stats[0][$group_field][$field]['ð']);
        foreach(self::ASSOCIATION_EMOJIS_CODE as $emoji_code => $emoji_image) {
            ?>
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <?= Functions::display_table_head($emoji_image); ?>
                    <tbody>
                        <?php
                        foreach($persons_stats as $person_stats) {
                            ?>
                            <tr>
                                <td><?=$person_stats['name'] . ' (sur ' . $person_stats[$group_field]['reactions_received_type'][$emoji_code] . ' reçues)'?></td>
                                <?php for($i=0; $i<$rows; $i++) {
                                    ?>
                                    <td class="<?=self::display_percentage_style($person_stats[$group_field][$pourcentage_field][$emoji_code][$i], $rows)?>">
                                        <?=$person_stats[$group_field][$field][$emoji_code][$i] . '(' . $person_stats[$group_field][$pourcentage_field][$emoji_code][$i] .'%)' ?>
                                    </td>
                                    <?php
                                } ?>
                            </tr>
                            <?php
                        } ?>
                    </tbody>
                </table>
            </div>
            <?php
        }
    }



}