<?php

require 'config.php';

$dossier = opendir($_CONFIG['paths']['messages']);

?>  <?php

while($nom_fichier = readdir($dossier))
{
    if(in_array($nom_fichier,['.', '..', 'stickers_used']))
        continue;

    $messages_path = $_CONFIG['paths']['messages'] . $nom_fichier . '/' . 'message_1.json';
    $fichier_json = json_decode(file_get_contents($messages_path));

    if(!property_exists($fichier_json, 'title'))
        continue;

    $titre_conversation = $fichier_json->title;
    $nombre_messages = count($fichier_json->messages);
    $date_premier_message = new DateTime('@' . round(end($fichier_json->messages)->timestamp_ms/1000));
    $date_ordonnée = $date_premier_message->format('D d F Y');

    echo '<a href="thread.php?group_name=' .  $nom_fichier .'">' . $titre_conversation . ' : ' . $nombre_messages . ' messages depuis le ' . $date_ordonnée . '<br>';

    // $fichier_json = mb_convert_encoding($fichier_json, 'HTML-ENTITIES', "UTF-8");
    ?>
    <!-- <div>
        <?php
        foreach($fichier_json->participants as $user)
        {
            echo $user->name . ' - ';
        }
        echo ': ' . count($fichier_json->messages);
        ?>
    </div> -->
    <?php
    // var_dump($fichier);
}

?>